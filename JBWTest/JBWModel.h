#import "JBWObservableObject.h"

typedef NS_ENUM(NSUInteger, JBWModelState) {
    JBWModelUnloaded,
    JBWModelLoaded,
    JBWModelLoading,
    JBWModelFailedLoading,
    JBWModelStateCount
};

@interface JBWModel : JBWObservableObject

- (void)load;

- (void)performLoading;

@end

@protocol JBWModelObserver <NSObject>
@optional
- (void)modelDidUnload:(JBWModel *)model;
- (void)modelDidLoad:(JBWModel *)model;
- (void)modelWillLoad:(JBWModel *)model;
- (void)modelDidFailLoading:(JBWModel *)model;

@end
