#import "JBWLoginView.h"

#import "UIButton+JBWExtensions.h"
#import "GIOMacro.h"

GIOStringConstantWithValue(kJBWLoginButtonTitle, "Login with Facebook");

@implementation JBWLoginView

- (void)awakeFromNib {
    [super awakeFromNib];
    
    UIButton *loginButton = self.loginButton;
    
    loginButton.backgroundColor=[UIColor colorWithRed:0.26 green:0.40 blue:0.70 alpha:1.0];
    loginButton.frame=CGRectMake(0,0,180,40);
    loginButton.center = self.center;
    loginButton.layer.cornerRadius = 5.0;
    [loginButton setTitle:kJBWLoginButtonTitle
                 forState:UIControlStateNormal
                withColor:[UIColor whiteColor]];
    
    [self addSubview:loginButton];
}


@end
