#import "JBWContext.h"

@interface JBWContext ()

@end

@implementation JBWContext

#pragma mark -
#pragma mark Class Methods

+ (instancetype)contextWithModel:(id)model {
    return [[self alloc] initWithModel:model];
}

#pragma mark -
#pragma mark Initializations and Deallocations

- (instancetype)initWithModel:(id)model {
    self = [super init];
    self.model = model;
    
    return self;
}

#pragma mark -
#pragma mark Public

- (void)execute {
    
}

- (void)cancel {
    
}

@end
