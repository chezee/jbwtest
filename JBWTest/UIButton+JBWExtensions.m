#import "UIButton+JBWExtensions.h"

@implementation UIButton (JBWExtensions)

- (void)setTitle:(NSString *)title forState:(UIControlState)state withColor:(UIColor *)color {
    [self setTitle: title forState: state];
    self.tintColor = color;
}

@end
