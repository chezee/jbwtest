#import "UIViewController+JBWExtensions.h"

@implementation UIViewController (JBWExtensions)

+ (instancetype)viewController {
    return [[self alloc] initWithNibName:[self nibName] bundle:nil];
}

+ (NSString *)nibName {
    return NSStringFromClass([self class]);
}


@end
