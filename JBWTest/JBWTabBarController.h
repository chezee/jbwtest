#import <UIKit/UIKit.h>

#import "JBWUser.h"
#import "JBWArrayModel.h"

@interface JBWTabBarController : UITabBarController <UITabBarDelegate, JBWUserObserver>
@property (nonatomic, strong) JBWUser *user;

@end
