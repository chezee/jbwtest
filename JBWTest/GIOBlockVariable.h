#import "GIOCompilerMacro.h"

#define GIOWeakify(variable) \
    __weak __typeof(variable) __GIOWeakified_##variable = variable;

#define GIOStrongify(variable) \
    GIOClangDiagnosticPushExpression("clang diagnostic ignored \"-Wshadow\""); \
    __strong __typeof(variable) variable = __GIOWeakified_##variable; \
    GIOClangDiagnosticPopExpression;

#define GIOEmptyResult

#define GIOStrongifyAndReturnIfNil(variable) \
    GIOStrongifyAndReturnResultIfNil(variable, GIOEmptyResult) \

#define GIOStrongifyAndReturnNilIfNil(variable) \
    GIOStrongifyAndReturnResultIfNil(variable, nil)

#define GIOStrongifyAndReturnResultIfNil(variable, result) \
    GIOStrongify(variable); \
    if (!variable) { \
        return result; \
    }

#define GIODispatchBlock(block, ...) \
    if (block) { \
        block (__VA_ARGS__); \
    }

#define GIOValueBlock(block, variable, ...) \
    if (block) { \
        variable = block(__VA_ARGS__); \
    }

#define GIOReturnSharedInstance(variable) \
    static id sharedInstance = nil; \
    static dispatch_once_t onceToken; \
    dispatch_once(&onceToken, ^{ \
        sharedInstance = variable; \
    }); \
    \
    return sharedInstance;

#define GIOReturnSharedInstanceWithBlock(block) \
    static id sharedInstance = nil; \
    static dispatch_once_t onceToken; \
    dispatch_once(&onceToken, ^{ \
        GIOValueBlock(block, sharedInstance); \
    }); \
    \
    return sharedInstance;


