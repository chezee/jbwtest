#import <UIKit/UIKit.h>

#import "JBWUser.h"

#import "JBWFeedView.h"

@interface JBWFeedViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>
@property (nonatomic, strong) JBWUser *user;
@property (strong, nonatomic) IBOutlet JBWFeedView *mainView;

@end
