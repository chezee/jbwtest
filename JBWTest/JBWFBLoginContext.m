#import "JBWFBLoginContext.h"

#import "GIOMacro.h"

static NSString * const kJBWFBPublicPermission = @"public_profile";
static NSString * const kJBWFBPhotoPermission = @"user_photos";
static NSString * const kJBWFBFeedPermission = @"user_posts";

@interface JBWFBLoginContext ()
@property (nonatomic, readonly)     FBSDKLoginManager   *loginManager;
@property (nonatomic, readonly)     FBSDKAccessToken    *accessToken;

- (void)loginAtFacebook;
- (void)completeLogin;

@end

@implementation JBWFBLoginContext

@dynamic accessToken;

#pragma mark -
#pragma mark Class Methods

+ (JBWUser *)user {
    FBSDKAccessToken *accessToken = [FBSDKAccessToken currentAccessToken];
    
    if (!accessToken) {
        return nil;
    }
    
    JBWUser *user = [JBWUser new];
    user.userId = accessToken.userID;
    
    return user;
}

#pragma mark -
#pragma mark Initializatiions and Deallocations

- (instancetype)initWithUser:(JBWUser *)user {
    self = [super initWithModel:user];
    
    return self;
}

#pragma mark -
#pragma mark Accessors

- (FBSDKLoginManager *)loginManager {
    return [FBSDKLoginManager new];
}

- (FBSDKAccessToken *)accessToken {
    return [FBSDKAccessToken currentAccessToken];
}

#pragma mark -
#pragma mark Public

- (void)execute {
    if (self.accessToken.userID) {
        [self completeLogin];
    } else {
        [self loginAtFacebook];
    }
}

#pragma mark -
#pragma mark Private

- (void)loginAtFacebook {
    FBSDKLoginManager *loginManager = self.loginManager;
    
    [loginManager logInWithReadPermissions:@[kJBWFBPublicPermission,
                                             kJBWFBPhotoPermission,
                                             kJBWFBFeedPermission]
                        fromViewController:nil
                                   handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
                                       if (error) {
                                           NSLog(@"Process error");
                                       } else if (result.isCancelled) {
                                           NSLog(@"Cancelled");
                                       } else {
                                           NSLog(@"Result: %@", result);
                                           [self completeLogin];
                                       }
                                   }];
}

- (void)completeLogin {
    NSLog(@"%@ - %@", NSStringFromClass([self class]), NSStringFromSelector(_cmd));
    FBSDKAccessToken *accessToken = self.accessToken;
    
    if (accessToken) {
        JBWUser *user = self.model;
        user.userId = accessToken.userID;
        
        user.state = JBWUserDidLoadFullInfo;
    }
}

@end
