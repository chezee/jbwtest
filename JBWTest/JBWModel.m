#import "JBWModel.h"

#import "GIOMacro.h"
#import "GIOGCD.h"

@implementation JBWModel

#pragma mark -
#pragma mark Public

- (void)load {
    @synchronized(self) {
        NSUInteger state = self.state;
        if (JBWModelLoaded == state || JBWModelLoading == state) {
            [self notifyOfState:state];
            
            return;
        }
        
        self.state = JBWModelLoading;
    }
    
    GIOWeakify(self);
    GIOAsyncPerformInBackgroundQueue(^{
        GIOStrongifyAndReturnIfNil(self);
        [self performLoading];
    });
}

- (void)performLoading {
    
}

#pragma mark -
#pragma mark JBWObservableObject

- (SEL)selectorForState:(NSUInteger)state {
    switch (state) {
        case JBWModelUnloaded:
            return @selector(modelDidUnload:);
            
        case JBWModelLoaded:
            return @selector(modelDidLoad:);
            
        case JBWModelLoading:
            return @selector(modelWillLoad:);
            
        case JBWModelFailedLoading:
            return @selector(modelDidFailLoading:);
            
        default:
            return [super selectorForState:state];
    }
}

@end
