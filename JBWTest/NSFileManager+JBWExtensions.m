#import "NSFileManager+JBWExtensions.h"

#import "GIOMacro.h"

@implementation NSFileManager (JBWExtensions)

+ (NSURL *)directoryPathWithType:(NSSearchPathDirectory)type {
    return [[[NSFileManager defaultManager] URLsForDirectory:type
                                                   inDomains:NSUserDomainMask] lastObject];
}

+ (NSURL *)documentsDirectoryURL {
    NSURL *directoryPath = [self directoryPathWithType:NSDocumentDirectory];
    
    GIOReturnSharedInstance(directoryPath);
}

+ (NSURL *)libraryDirectoryURL {
    NSURL *directoryPath = [self directoryPathWithType:NSLibraryDirectory];
    
    GIOReturnSharedInstance(directoryPath);
}

+ (NSURL *)applicationDirectoryURL {
    NSURL *directoryPath = [self directoryPathWithType:NSApplicationSupportDirectory];
    
    GIOReturnSharedInstance(directoryPath);
}

@end
