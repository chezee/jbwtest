#import "JBWFBContext.h"

#import "JBWModel.h"
#import "GIOMacro.h"
#import "JBWUser.h"
#import "JBWFeed.h"

@interface JBWFBContext ()
@property (nonatomic, strong) FBSDKGraphRequestConnection *graphRequestConnection;

@end

@implementation JBWFBContext

#pragma mark -
#pragma mark Accessors

- (void)setGraphRequestConnection:(FBSDKGraphRequestConnection *)graphRequestConnection {
    if (_graphRequestConnection != graphRequestConnection) {
        [_graphRequestConnection cancel];
        _graphRequestConnection = graphRequestConnection;
        //[_graphRequestConnection start];
    }
}


#pragma mark -
#pragma mark Public

- (void)resultHandler:(id)result {
    
}

- (NSString *)graphPath {
    return nil;
}

- (NSDictionary *)requestParameters {
    return nil;
}

- (FBSDKGraphRequest *)graphRequest {
    return [[FBSDKGraphRequest alloc] initWithGraphPath:self.graphPath
                                             parameters:self.requestParameters];
}

- (void)execute {
    if ([FBSDKAccessToken currentAccessToken]) {
        GIOWeakify(self);
        self.graphRequestConnection = [self.graphRequest startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error) {
            GIOStrongifyAndReturnIfNil(self);
            if (!error) {
                [self resultHandler:result];
            } else {
                NSLog(@"Error: %@", error);
            }
        }];
    }
}

@end

