#import "JBWContext.h"

#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>

@interface JBWFBContext : JBWContext

- (void)resultHandler:(id)result;

- (NSString *)graphPath;
- (NSDictionary *)requestParameters;
- (FBSDKGraphRequest *)graphRequest;

@end
