#import "JBWArrayModel.h"
#import <UIKit/UIKit.h>

@interface JBWFeed : JBWArrayModel
@property (nonatomic, readonly) NSString *path;

- (NSString *)path;

- (void)save;

@end
