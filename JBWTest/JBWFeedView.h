#import <UIKit/UIKit.h>
#import "JBWArrayModel.h"

@interface JBWFeedView : UIView
@property (nonatomic, strong) JBWArrayModel *model;
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end
