#import <Foundation/Foundation.h>

typedef void (^GIOGCDBlock)(void);

typedef NSUInteger GIODelayInSeconds;

typedef enum {
    GIODispatchQueueHighPriority = QOS_CLASS_USER_INITIATED,
    GIODispatchQueueDefaultPriority = QOS_CLASS_DEFAULT,
    GIODispatchQueueLowPriority = QOS_CLASS_UTILITY,
    GIODispatchQueueBackgroundPriority = QOS_CLASS_BACKGROUND
} GIODispatchQueuePriority;

void GIOAsyncPerformInBackgroundQueue(GIOGCDBlock block);
void GIOAsyncPerformInQueue(GIODispatchQueuePriority type, GIOGCDBlock block);
void GIOAsyncPerformInMainQueue(GIOGCDBlock block);

void GIOSyncPerformInBackgroundQueue(GIOGCDBlock block);
void GIOSyncPerformInQueue(GIODispatchQueuePriority type, GIOGCDBlock block);
void GIOSyncPerformInMainQueue(GIOGCDBlock block);

void GIODispatchAfter(NSTimeInterval delay, GIODispatchQueuePriority type, GIOGCDBlock block);

dispatch_queue_t GIOGetDispatchGlobalQueueWithType(GIODispatchQueuePriority type);

