#import "UIWindow+JBWExtensions.h"

@implementation UIWindow (JBWExtensions)

+ (instancetype)window {
    return [[self alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
}


@end
