#import <UIKit/UIKit.h>

#import "JBWArrayModel.h"
#import "JBWModel.h"

typedef NS_ENUM(NSUInteger, JBSUserState) {
    JBWUserDidLoadFullInfo = JBWModelStateCount,
    JBWUserStateCount
};

@interface JBWUser : JBWModel <NSCoding>
@property (nonatomic, copy) NSString *userId;
@property (nonatomic, strong) JBWArrayModel *feed;

@end

@protocol JBWUserObserver <NSObject, JBWModelObserver>

@optional
- (void)userDidLoadFullInfo:(JBWUser *)user;

@end
