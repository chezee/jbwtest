#import "UITableView+JBWExtensions.h"

#import "JBWFeedViewController.h"
#import "JBWFeedCell.h"

#import "JBWFeedContext.h"
#import "JBWFeedPost.h"
#import "JBWArrayModel.h"

#import "GIOMacro.h"

GIOStringConstantWithValue(kFeedCellIdentifier, "FeedCellIdentifier");

@interface JBWFeedViewController ()
@property (nonatomic, strong) JBWFeedContext *feedContext;
@property (nonatomic, strong) JBWArrayModel *feed;

- (IBAction)onLogOutButtonClicked:(id)sender;

@end

@implementation JBWFeedViewController

@dynamic feed;

#pragma mark -
#pragma mark Accessors

- (void)setUser:(JBWUser *)user {
    if (user != _user) {
        [_user removeObserver:self];
        
        _user = user;
        
        [_user addObserver:self];
    }
}

- (void)setFeedContext:(JBWFeedContext *)feedContext {
    NSLog(@"%@ - %@", NSStringFromClass([self class]), NSStringFromSelector(_cmd));
    if (feedContext != _feedContext) {
        [_feedContext cancel];
        
        _feedContext = feedContext;
        
        _feedContext.model = self.feed;
        
        [_feedContext execute];
    }
}

- (JBWArrayModel *)feed {
    return self.feedContext.model;
}

#pragma mark -
#pragma mark Private

- (IBAction)onLogOutButtonClicked:(id)sender {
    [[FBSDKLoginManager new] logOut];
    [self.navigationController popToRootViewControllerAnimated:YES];
}


#pragma mark -
#pragma mark Lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UINavigationItem *navigationItem = self.navigationItem;
    UIBarButtonItem *logoutButton = [[UIBarButtonItem alloc] initWithTitle:@"Logout"
                                                                     style:UIBarButtonItemStylePlain
                                                                    target:self
                                                                    action:@selector(onLogOutButtonClicked:)];
    [navigationItem setLeftBarButtonItem:logoutButton animated:NO];
    
    self.feedContext = [JBWFeedContext new];
    self.mainView.model = self.feed;
    [self.mainView.tableView registerClass:[JBWFeedCell class] forCellReuseIdentifier:kFeedCellIdentifier];
}

#pragma mark -
#pragma mark UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.feed.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    JBWFeedCell *cell = [tableView reusableCellWithClass:[JBWFeedCell class]];
    cell.post = self.mainView.model.objects[indexPath.row];
    
    return cell;
}

@end
