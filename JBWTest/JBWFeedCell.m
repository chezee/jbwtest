#import "JBWFeedCell.h"

@implementation JBWFeedCell

#pragma mark -
#pragma mark Initializations and Deallocations


#pragma mark -
#pragma mark Accessors

- (void)setUser:(JBWFeedPost *)post {
    if (_post != post) {
        
        _post = post;
        
        [self fillWithPostModel:post];
    }
}

#pragma mark -
#pragma mark Public

- (void)fillWithPostModel:(JBWFeedPost *)post {
    self.userLabel.text = post.message;
    self.userImageView.text = post.story;
}

@end
