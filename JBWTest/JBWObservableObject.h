#import <Foundation/Foundation.h>

@interface JBWObservableObject : NSObject
@property (nonatomic, assign) NSUInteger  state;
@property (nonatomic, readonly) NSSet *observersSet;

- (void)addObserver:(id)observer;
- (void)addObservers:(NSArray *)observers;

- (void)removeObserver:(id)observer;
- (void)removeObservers:(NSArray *)observers;

- (BOOL)containsObserver:(id)observer;

- (SEL)selectorForState:(NSUInteger)state;
- (SEL)selectorForState:(NSUInteger)state withObject:(id)object;

- (void)setState:(NSUInteger)state withObject:(id)object;

- (void)notifyOfState:(NSUInteger)state;
- (void)notifyOfState:(NSUInteger)state object:(id)object;

- (void)performBlockWithNotifications:(void(^)(void))block;
- (void)performBlockWithoutNotifications:(void(^)(void))block;

@end
