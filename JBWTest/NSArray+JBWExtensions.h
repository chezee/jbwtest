#import <Foundation/Foundation.h>

@interface NSArray (JBWExtensions)

+ (instancetype)objectsWithCount:(NSUInteger)count block:(id(^)())block;

- (id)objectWithClass:(Class)cls;

@end
