#import "JBWLoginViewController.h"
#import "JBWTabBarController.h"

#import "JBWFBLoginContext.h"

#import "GIOGCD.h"
#import "UIViewController+JBWExtensions.h"

@interface JBWLoginViewController ()
@property (nonatomic, strong) JBWFBLoginContext *loginContext;

- (void)pushViewControllerWithUser:(JBWUser *)user withAnimation:(BOOL)animation;

@end

@implementation JBWLoginViewController

#pragma mark -
#pragma mark Accessors

- (void)setUser:(JBWUser *)user {
    if (_user != user) {
        [_user removeObserver:self];
        
        _user = user;
        
        [_user addObserver:self];
    }
}

#pragma mark -
#pragma mark Lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    
    JBWUser *user = [JBWFBLoginContext user];
    
    if (user) {
        [self pushViewControllerWithUser:user withAnimation:NO];
    }
    
}

#pragma mark -
#pragma mark Private

- (void)pushViewControllerWithUser:(JBWUser *)user withAnimation:(BOOL)animation {
    JBWTabBarController *controller = [JBWTabBarController viewController];
    controller.user = user;

    [self.navigationController pushViewController:controller animated:animation];
}

#pragma mark -
#pragma mark Public

- (IBAction)onLoginButtonClicked:(id)sender {
    NSLog(@"%@ - %@", NSStringFromClass([self class]), NSStringFromSelector(_cmd));
    JBWUser *user  = [JBWUser new];
    JBWFBLoginContext *loginContext = [[JBWFBLoginContext alloc] initWithUser:user];

    self.user = user;
    self.loginContext = loginContext;
    
    [loginContext execute];
}

#pragma mark -
#pragma mark JBWUserObserver

- (void)userDidLoadFullInfo:(JBWUser *)user {
    NSLog(@"%@ - %@", NSStringFromClass([self class]), NSStringFromSelector(_cmd));
    GIOAsyncPerformInMainQueue(^{
        [self pushViewControllerWithUser:user withAnimation:YES];
    });
}

@end
