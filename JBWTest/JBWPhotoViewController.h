#import <UIKit/UIKit.h>

#import "JBWUser.h"

#import "JBWPhotoView.h"

@interface JBWPhotoViewController : UIViewController <UICollectionViewDelegate, UICollectionViewDataSource>
@property (nonatomic, strong) JBWUser *user;
@property (nonatomic, strong) IBOutlet JBWPhotoView *mainView;

@end
