#import <Foundation/Foundation.h>

@interface NSFileManager (JBWExtensions)

+ (NSURL *)documentsDirectoryURL;

+ (NSURL *)libraryDirectoryURL;

+ (NSURL *)applicationDirectoryURL;

- (void)copyItemAtURL:(NSURL *)url toURL:(NSURL *)toURL;

@end
