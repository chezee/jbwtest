#import <UIKit/UIKit.h>
#import "JBWModel.h"

@interface JBWFeedPost : JBWModel
@property (nonatomic, strong) NSString *message;
@property (nonatomic, strong) NSString *story;
@property (nonatomic, strong) NSString *createdTime;

@end
