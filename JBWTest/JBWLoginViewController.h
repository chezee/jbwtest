#import <UIKit/UIKit.h>

#import "JBWUser.h"

@interface JBWLoginViewController : UIViewController <JBWUserObserver>
@property (nonatomic, strong) JBWUser *user;

- (IBAction)onLoginButtonClicked:(id)sender;

@end
