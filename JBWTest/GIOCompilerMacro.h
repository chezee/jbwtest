#define GIOClnagDiagnosticPush _Pragma("clang diagnostic push")
#define GIOClangDiagnosticPop _Pragma("clang diagnostic pop")

#define GIOClangDiagnosticPushExpression(key) \
    GIOClnagDiagnosticPush; \
    _Pragma(key)

#define GIOClangDiagnosticPopExpression GIOClangDiagnosticPop
