#import "JBWFeed.h"

#import "NSFileManager+JBWExtensions.h"

@interface JBWFeed ()

- (void)subscribeAtAppNotifications:(NSArray *)notifications;

- (NSArray *)savedFeed;

@end

@implementation JBWFeed

#pragma mark -
#pragma mark Initializations and Deallocations

- (instancetype)init {
    self = [super init];
    
    [self subscribeAtAppNotifications:@[UIApplicationWillResignActiveNotification,
                                        UIApplicationWillTerminateNotification]];
    return self;
}

#pragma mark -
#pragma mark Public

- (NSString *)path {
    NSString *fileName = [NSString stringWithFormat:@"%@.plist", NSStringFromClass([self class])];
    NSURL *appDirectory = [NSFileManager documentsDirectoryURL];
    
    return [[appDirectory path] stringByAppendingString:fileName];
}

- (void)save {
    [NSKeyedArchiver archiveRootObject:self.objects toFile:self.path];
}

#pragma mark -
#pragma mark Private

- (void)subscribeAtAppNotifications:(NSArray *)notifications {
    NSNotificationCenter *noticationCenter = [NSNotificationCenter defaultCenter];
    
    for (id notification in notifications) {
        [noticationCenter addObserver:self
                             selector:@selector(save)
                                 name:notification
                               object:nil];
    }
}

- (NSArray *)savedFeed {
    return [NSKeyedUnarchiver unarchiveObjectWithFile:self.path];
}

- (void)performLoading {
    NSArray *feed = self.savedFeed;
    [self performBlockWithoutNotifications:^{
        [self addObjects:feed];
    }];
    
    self.state = JBWModelLoaded;
}

@end
