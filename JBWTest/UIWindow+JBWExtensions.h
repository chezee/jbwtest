#import <UIKit/UIKit.h>

@interface UIWindow (JBWExtensions)

+ (instancetype)window;

@end
