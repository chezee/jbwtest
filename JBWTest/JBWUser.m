#import "JBWUser.h"

#import "GIOMacro.h"

GIOStringConstantWithValue(kUserID, "kGIOCoderUserId");

@implementation JBWUser

#pragma mark -
#pragma mark Accessors

- (void)setUserId:(NSString *)userId {
    if (_userId != userId) {
        _userId = userId;
    }
}

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super init];
    if (self) {
#define decode(field, key) field = [aDecoder decodeObjectForKey:key]
        decode(self.userId, kUserID);
#undef decode
    }
    
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder {
#define encode(field, key) [aCoder encodeObject:field forKey:key]
    encode(self.userId, kUserID);
#undef encode
}

- (SEL)selectorForState:(NSUInteger)state {
    SEL selector = NULL;
    
    switch (state) {
        case JBWUserDidLoadFullInfo:
            selector = @selector(userDidLoadFullInfo:);
            break;
            
        default:
            selector = [super selectorForState:state];
            break;
    }
    
    return selector;
}

@end
