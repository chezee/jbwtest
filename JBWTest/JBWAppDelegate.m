#import "UIWindow+JBWExtensions.h"
#import "UIViewController+JBWExtensions.h"

#import "JBWAppDelegate.h"
#import "JBWLoginViewController.h"

#import <FBSDKCoreKit/FBSDKCoreKit.h>

@interface JBWAppDelegate ()

@end

@implementation JBWAppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    UIWindow *window = [UIWindow window];
    self.window = window;
    
    JBWLoginViewController *controller = [JBWLoginViewController viewController];
    UINavigationController *navigationController = [[UINavigationController alloc]
                                                    initWithRootViewController: controller];

    window.rootViewController = navigationController;
    [window makeKeyAndVisible];
    
    return [[FBSDKApplicationDelegate sharedInstance] application:application
                                    didFinishLaunchingWithOptions:launchOptions];
}

- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url options:(NSDictionary<UIApplicationOpenURLOptionsKey,id> *)options {
    FBSDKApplicationDelegate *sharedInstance = [FBSDKApplicationDelegate sharedInstance];
    
    BOOL handled = [sharedInstance application:application
                                       openURL:url
                             sourceApplication:options[UIApplicationOpenURLOptionsSourceApplicationKey]
                                    annotation:options[UIApplicationOpenURLOptionsAnnotationKey]];
    
    return handled;
}



- (void)applicationWillResignActive:(UIApplication *)application {

}


- (void)applicationDidEnterBackground:(UIApplication *)application {
   
}


- (void)applicationWillEnterForeground:(UIApplication *)application {
   
}


- (void)applicationDidBecomeActive:(UIApplication *)application {
  
}


- (void)applicationWillTerminate:(UIApplication *)application {
    
}

@end
