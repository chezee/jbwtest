#import "JBWContext.h"

#import "JBWUser.h"

@interface JBWFBLoginContext : JBWContext

+ (JBWUser *)user;

- (instancetype)initWithUser:(JBWUser *)user;

@end
