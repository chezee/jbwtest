#import "JBWFeedContext.h"

#import "JBWArrayModel.h"
#import "JBWFeedPost.h"

@interface JBWFeedContext ()
@property (nonatomic, strong) JBWArrayModel *feed;

@end

@implementation JBWFeedContext

#pragma mark -
#pragma mark Accessors

- (JBWArrayModel *)feed {
    return (JBWArrayModel *)self.model;
}

#pragma mark -
#pragma mark Public

- (NSString *)graphPath {
    return @"me/feed";
}

- (NSDictionary *)requestParameters {
    return @{@"with": @"location"};;
}

- (FBSDKGraphRequest *)graphRequest {
    return [[FBSDKGraphRequest alloc] initWithGraphPath:self.graphPath
                                             parameters:self.requestParameters
                                             HTTPMethod:@"GET"];
}

- (void)resultHandler:(NSDictionary *)result {
    NSArray *array = [result objectForKey:@"data"];
    JBWArrayModel *feed = self.feed;
    
    for (NSDictionary *object in array) {
        JBWFeedPost *post = [JBWFeedPost new];
        if (object[@"message"]) {
            post.message = object[@"message"];
        }
        post.story = object[@"story"];
        post.createdTime = object[@"created_time"];
        
        [feed performBlockWithoutNotifications:^{
            [feed addObject:post];
        }];
    }
    
    feed.state = JBWModelLoaded;
}


@end
