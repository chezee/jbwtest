#import <UIKit/UIKit.h>
#import "JBWFeedPost.h"

@interface JBWFeedCell : UITableViewCell
@property (nonatomic, strong) IBOutlet UILabel *userLabel;
@property (nonatomic, strong) IBOutlet UILabel *userImageView;

@property (nonatomic, strong) JBWFeedPost *post;

@end
