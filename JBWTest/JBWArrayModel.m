#import "JBWArrayModel.h"

@interface JBWArrayModel()
@property (nonatomic, strong) NSMutableArray *array;

@end

@implementation JBWArrayModel

@dynamic count;

- (instancetype)init {
    self = [super init];
    if (self) {
        self.array = [NSMutableArray array];
    }
    
    return self;
}

#pragma mark -
#pragma mark Accessors

- (NSArray *)objects {
    return [self.array copy];
}

#pragma mark -
#pragma mark Public

- (NSUInteger)count {
    return self.array.count;
}

- (void)addObject:(id)object {
    [self insertObject:object atIndex:self.count];
}

- (void)insertObject:(id)object atIndex:(NSUInteger)index {
    [self.array insertObject:object atIndex:index];
}

- (void)addObjects:(NSArray *)objects {
    for (id object in objects) {
        [self.array addObject:object];
    }
}

- (id)objectAtIndex:(NSUInteger)index {
    return self.array[index];
}

- (NSUInteger)indexOfObject:(id)object {
    return [self.array indexOfObject:object];
}

#pragma mark -
#pragma mark NSFastEnumeration

- (NSUInteger)countByEnumeratingWithState:(NSFastEnumerationState *)state
                                  objects:(id __unsafe_unretained [])buffer
                                    count:(NSUInteger)length
{
    return [self.array countByEnumeratingWithState:state objects:buffer count:length];
}

@end
