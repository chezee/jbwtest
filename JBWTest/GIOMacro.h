#import "GIOBlockVariable.h"

#ifndef GIOMacro_h
#define GIOMacro_h

#define GIOViewControllerBaseViewPropertyWithGetter(viewControllerClass, baseViewClass, propertyName) \
    @interface viewControllerClass (__GIOPrivatBaseView) \
    GIODefineBaseViewProrety(propertyName, baseViewClass)\
    \
    @end \
    \
    @implementation viewControllerClass (__GIOPrivatBaseView) \
    \
    @dynamic propertyName; \
    \
    GIOBaseViewGetterSyntesize(propertyName, baseViewClass)\
    \
    @end

#define GIODefineBaseViewProrety(propertyName, viewClass) \
    @property (nonatomic, readonly) viewClass *propertyName;

#define GIOStringConstantWithValue(name, value) \
    static NSString * const name = @#value

#endif

