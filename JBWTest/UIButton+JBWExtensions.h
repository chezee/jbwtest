#import <UIKit/UIKit.h>

@interface UIButton (JBWExtensions)

- (void)setTitle:(NSString *)title forState:(UIControlState)state withColor:(UIColor *)color;

@end
