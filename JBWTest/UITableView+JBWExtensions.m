#import "UITableView+JBWExtensions.h"

#import "UINib+JBWExtensions.h"

#import "GIOMacro.h"

@implementation UITableView (JBWExtensions)

- (id)dequeueReusableCellWithClass:(Class)cls {
    return [self dequeueReusableCellWithIdentifier:NSStringFromClass(cls)];
}

- (id)reusableCellWithClass:(Class)cls {
    id cell = [self dequeueReusableCellWithClass:cls];
    
    if (!cell) {
        cell = [UINib objectWithClass:cls];
    }
    
    return cell;
}

- (void)updateWithBlock:(JBWTableViewUpdateBlock)block {
    if (!block) {
        return;
    }
    
    [self beginUpdates];
    GIODispatchBlock(block);
    [self endUpdates];
}

@end
