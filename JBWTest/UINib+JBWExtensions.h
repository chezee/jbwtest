#import <UIKit/UIKit.h>

@interface UINib (JBWExtensions)
+ (UINib *)nibWithClass:(Class)cls;
+ (UINib *)nibWithClass:(Class)cls bundle:(NSBundle *)bundle;

+ (id)objectWithClass:(Class)cls;
+ (id)objectWithClass:(Class)cls bundle:(NSBundle *)bundle;
+ (id)objectWithClass:(Class)cls bundle:(NSBundle *)bundle withOwner:(id)owner withOptions:(NSDictionary *)options;

- (id)objectWithClass:(Class)cls;
- (id)objectWithClass:(Class)cls withOwner:(id)owner;
- (id)objectWithClass:(Class)cls withOwner:(id)owner withOptions:(NSDictionary *)options;

- (NSArray *)objectsWithOwner:(id)owner withOptions:(NSDictionary *)options;

@end
