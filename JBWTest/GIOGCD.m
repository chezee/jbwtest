#import "GIOGCD.h"

void GIOAsyncPerformInBackgroundQueue(GIOGCDBlock block) {
    GIOAsyncPerformInQueue(GIODispatchQueueBackgroundPriority, block);
}

void GIOAsyncPerformInQueue(GIODispatchQueuePriority type, GIOGCDBlock block) {
    dispatch_async(GIOGetDispatchGlobalQueueWithType(type), block);
}

void GIOAsyncPerformInMainQueue(GIOGCDBlock block) {
    dispatch_async(dispatch_get_main_queue(), block);
}

void GIOSyncPerformInBackgroundQueue(GIOGCDBlock block) {
    GIOSyncPerformInQueue(GIODispatchQueueBackgroundPriority, block);
}

void GIOSyncPerformInQueue(GIODispatchQueuePriority type, GIOGCDBlock block) {
    dispatch_sync(GIOGetDispatchGlobalQueueWithType(type), block);
}

void GIOSyncPerformInMainQueue(GIOGCDBlock block) {
    if ([NSThread isMainThread]) {
        block();
    } else {
        dispatch_sync(dispatch_get_main_queue(), block);
    }
}

dispatch_queue_t GIOGetDispatchGlobalQueueWithType(GIODispatchQueuePriority type) {
    return dispatch_get_global_queue(type, 0);
}

void GIOCancelDispatchBlock(GIOGCDBlock block) {
    if (!block) {
        return;
    }
    
    void (^executedBlock)(BOOL) = (void(^)(BOOL))block;
    executedBlock(YES);
}


