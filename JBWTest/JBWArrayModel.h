#import "JBWModel.h"

@interface JBWArrayModel : JBWModel <NSFastEnumeration>
@property (nonatomic, readonly) NSUInteger count;
@property (nonatomic, copy, readonly) NSArray *objects;

- (void)addObject:(id)object;
- (void)addObjects:(NSArray *)objects;

- (id)objectAtIndex:(NSUInteger)index;
- (NSUInteger)indexOfObject:(id)object;

@end
