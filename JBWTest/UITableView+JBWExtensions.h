#import <UIKit/UIKit.h>

typedef void (^JBWTableViewUpdateBlock)(void);

@interface UITableView (JBWExtensions)

- (id)dequeueReusableCellWithClass:(Class)cls;

- (id)reusableCellWithClass:(Class)cls;

- (void)updateWithBlock:(JBWTableViewUpdateBlock)block;

@end
