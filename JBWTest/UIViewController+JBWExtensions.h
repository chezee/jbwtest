#import <UIKit/UIKit.h>

@interface UIViewController (JBWExtensions)

+ (instancetype)viewController;

@end
