#import "JBWTabBarController.h"

#import "JBWFeedViewController.h"
#import "JBWPhotoViewController.h"

#import "JBWFBLoginContext.h"
#import "JBWFeedContext.h"

#import "GIOGCD.h"
#import "UIViewController+JBWExtensions.h"

@interface JBWTabBarController ()
@property (strong, nonatomic) JBWArrayModel *model;
@property (strong, nonatomic) JBWFeedContext *feed;

@end

@implementation JBWTabBarController

#pragma mark -
#pragma mark Accessors

- (void)setUser:(JBWUser *)user {
    if (_user != user) {
        [_user removeObserver:self];
        
        _user = user;
        
        [_user addObserver:self];
    }
}

- (void)setFeed:(JBWFeedContext *)feed {
    NSLog(@"%@ - %@", NSStringFromClass([self class]), NSStringFromSelector(_cmd));
    if (feed != _feed) {
        [_feed cancel];
        
        _feed = feed;
        
        [_feed execute];
    }
}


#pragma mark -
#pragma mark Lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.user = [JBWFBLoginContext user];
    self.feed = [JBWFeedContext new];
    
    if (self.user) {
        [self onConfigureTabBar];
    }    
}

#pragma mark -
#pragma mark Private

- (void)onConfigureTabBar {
    UIImage *feedImage = [UIImage imageNamed:@"Feed"];
    UITabBarItem *feedItem = [[UITabBarItem alloc] initWithTitle:@"" image:feedImage tag:0];
    UIImage *photoImage = [UIImage imageNamed:@"Photo"];
    UITabBarItem *photoItem = [[UITabBarItem alloc] initWithTitle:@"" image:photoImage tag:1];
    
    JBWFeedViewController *feedController = [JBWFeedViewController viewController];
    JBWPhotoViewController *photoController = [JBWPhotoViewController viewController];
    
    feedController.tabBarItem = feedItem;
    [feedController setUser:self.user];
    photoController.tabBarItem = photoItem;
    [photoController setUser:self.user];
    
    [self setViewControllers:@[feedController, photoController]];
}

#pragma mark -
#pragma mark JBWUserObserver

- (void)userDidLoadFullInfo:(JBWUser *)user {
    NSLog(@"%@ - %@", NSStringFromClass([self class]), NSStringFromSelector(_cmd));
    GIOAsyncPerformInMainQueue(^{
        [self onConfigureTabBar];
    });

}
@end
